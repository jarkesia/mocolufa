/*
  dualMocoLUFA Project

  Copyright (C) 2013 by morecat_lab
                2023 by Jari Suominen
                   
  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
         
    http://morecatlab.akiba.coocan.jp
    http://www.tasankokaiku.com

  based on LUFA-100807
          
  LUFA Library
  Copyright (C) Dean Camera, 2010.
  dean [at] fourwalledcubicle [dot] com
  www.fourwalledcubicle.com

*/


/** \file
 *
 *  Header file for Descriptors.c.
 */
 
#ifndef _DESCRIPTORS_H_
#define _DESCRIPTORS_H_

	/* Includes: */
		#include <avr/pgmspace.h>

		#include "../../LUFA/Drivers/USB/USB.h"
		#include <LUFA/Drivers/USB/Class/CDC.h>
		#include <LUFA/Drivers/USB/Class/MIDI.h>
		
	/* Product-specific definitions: */
		#define ARDUINO_UNO_PID			0x0001
		#define ARDUINO_MEGA2560_PID		0x0010
		#define ATMEL_LUFA_DEMO_PID		0x204B

	/* Macros for CDC */
		/** Endpoint number of the CDC device-to-host notification IN endpoint. */
		#define CDC_NOTIFICATION_EPNUM         2
		/** Endpoint number of the CDC device-to-host data IN endpoint. */
		#define CDC_TX_EPNUM                   3	
		/** Endpoint number of the CDC host-to-device data OUT endpoint. */
		#define CDC_RX_EPNUM                   4	
		/** Size in bytes of the CDC device-to-host notification IN endpoint. */
		#define CDC_NOTIFICATION_EPSIZE        8
		/** Size in bytes of the CDC data IN and OUT endpoints. */
		#define CDC_TXRX_EPSIZE                64	

	/* Macros for MIDI */
		/** Endpoint number of the MIDI streaming data IN endpoint, for device-to-host data transfers. */
		#define MIDI_STREAM_IN_EPNUM        2
		/** Endpoint number of the MIDI streaming data OUT endpoint, for host-to-device data transfers. */
		#define MIDI_STREAM_OUT_EPNUM       1
		/** Endpoint size in bytes of the Audio isochronous streaming data IN and OUT endpoints. */
		#define MIDI_STREAM_EPSIZE          64
		
	/* Type Defines for CDC */
		/** Type define for the device configuration descriptor structure. This must be defined in the
		 *  application code, as the configuration descriptor contains several sub-descriptors which
		 *  vary between devices, and which describe the device's usage to the host.
		 */
		typedef struct
		{
			USB_Descriptor_Configuration_Header_t    Config;
			USB_Descriptor_Interface_t               CDC_CCI_Interface;
			CDC_FUNCTIONAL_DESCRIPTOR(2)             CDC_Functional_IntHeader;
			CDC_FUNCTIONAL_DESCRIPTOR(1)             CDC_Functional_AbstractControlManagement;
			CDC_FUNCTIONAL_DESCRIPTOR(2)             CDC_Functional_Union;
			USB_Descriptor_Endpoint_t                CDC_NotificationEndpoint;
			USB_Descriptor_Interface_t               CDC_DCI_Interface;
			USB_Descriptor_Endpoint_t                CDC_DataOutEndpoint;
			USB_Descriptor_Endpoint_t                CDC_DataInEndpoint;
		} USB_Descriptor_ConfigurationCDC_t;

	/* Type Defines for MIDI */
		typedef struct
		{
			USB_Descriptor_Configuration_Header_t Config;
			USB_Descriptor_Interface_t            Audio_ControlInterface;
			USB_Audio_Interface_AC_t              Audio_ControlInterface_SPC;
			USB_Descriptor_Interface_t            Audio_StreamInterface;
			USB_MIDI_AudioInterface_AS_t          Audio_StreamInterface_SPC;
			USB_MIDI_In_Jack_t                    MIDI_In_Jack_Emb;
			USB_MIDI_In_Jack_t                    MIDI_In_Jack_Ext;
			USB_MIDI_Out_Jack_t                   MIDI_Out_Jack_Emb;
			USB_MIDI_Out_Jack_t                   MIDI_Out_Jack_Ext;
			USB_Audio_StreamEndpoint_Std_t        MIDI_In_Jack_Endpoint;
			USB_MIDI_Jack_Endpoint_t              MIDI_In_Jack_Endpoint_SPC;
			USB_Audio_StreamEndpoint_Std_t        MIDI_Out_Jack_Endpoint;
			USB_MIDI_Jack_Endpoint_t              MIDI_Out_Jack_Endpoint_SPC;
		} USB_Descriptor_ConfigurationMIDI_t;
		
	/* Function Prototypes: */
		uint16_t CALLBACK_USB_GetDescriptor(const uint16_t wValue,
		                                    const uint8_t wIndex,
		                                    void** const DescriptorAddress) ATTR_WARN_UNUSED_RESULT ATTR_NON_NULL_PTR_ARG(3);
#endif
