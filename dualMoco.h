/*
  dualMocoLUFA Project

  Copyright (C) 2013 by morecat_lab
                2023 by Jari Suominen
                   
  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
         
    http://morecatlab.akiba.coocan.jp
    http://www.tasankokaiku.com

  based on LUFA-100807
          
  LUFA Library
  Copyright (C) Dean Camera, 2010.
  dean [at] fourwalledcubicle [dot] com
  www.fourwalledcubicle.com

*/

/** \file
 *
 *  Header file for doubleMonaka.c
 */

#ifndef _DUAL_MOCO_H_
#define _DUAL_MOCO_H_

	/* Includes: */
		#include <avr/io.h>
		#include <avr/wdt.h>
		#include <avr/interrupt.h>
		#include <avr/power.h>

		#include "Descriptors.h"

		#include "Lib/LightweightRingBuff.h"

		#include <LUFA/Version.h>
		#include <LUFA/Drivers/Board/LEDs.h>
		#include <LUFA/Drivers/Peripheral/Serial.h>
		#include <LUFA/Drivers/USB/USB.h>
		#include <LUFA/Drivers/USB/Class/CDC.h>
		
	/* Macros: */
		/** LED mask for the library LED driver, to indicate TX activity. */
		#define LEDMASK_TX               LEDS_LED1

		/** LED mask for the library LED driver, to indicate RX activity. */
		#define LEDMASK_RX               LEDS_LED2
		
		/** LED mask for the library LED driver, to indicate that an error has occurred in the USB interface. */
		#define LEDMASK_ERROR            (LEDS_LED1 | LEDS_LED2)
		
		/** LED mask for the library LED driver, to indicate that the USB interface is busy. */
		#define LEDMASK_BUSY             (LEDS_LED1 | LEDS_LED2)		
		
		typedef uint8_t uchar;
		#define HW_CDC_BULK_OUT_SIZE     8
		#define HW_CDC_BULK_IN_SIZE      8

		#define	TRUE			1
		#define	FALSE			0

	/* Function Prototypes: */
		void SetupHardware(void);
		void processSerial(void);
		void processMIDI(void);

		void EVENT_USB_Device_Connect(void);
		void EVENT_USB_Device_Disconnect(void);
		void EVENT_USB_Device_ConfigurationChanged(void);
		void EVENT_USB_Device_UnhandledControlRequest(void);
		
		void EVENT_CDC_Device_LineEncodingChanged(USB_ClassInfo_CDC_Device_t* const CDCInterfaceInfo);
		void EVENT_CDC_Device_ControLineStateChanged(USB_ClassInfo_CDC_Device_t* const CDCInterfaceInfo);		

		uchar parseSerialMidiMessage(uchar);
		void parseUSBMidiMessage(uchar *);
	/* shared variable */
		extern uchar mocoMode;

#endif /* _DUAL_MOCO_H_ */
